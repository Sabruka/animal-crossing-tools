# README #

### How To Use ###

`python3 run.py $tool`

### Tool: Random Town Tune Generator (rttg) ###

- Generates a random 16-note town tune.
- Can start from a base tune with missing notes, or from scratch: `python3 ./run.py rttg base x` where x is a string of 16 characters, with periods used to denote the notes that need to be "completed" by the algorithm.
- Randomly inserts pauses and "continues" where musically pleasing.
- Allows you to generate multiple songs: `python3 ./run.py rttg x` where x is the number of songs to generate.
- Once generated, songs will automatically play with a sample rate of 44100.
- First note generated randomly, subsequent notes generated using a series of randomly-selected profiles.

### Bugs ###

- No input validation is performed.

Please file all bugs to jonathan.pitre+actp@gmail.com
- 
