import random

NOUN_DICT = {
    2: ['ad', 'ab', 'ax', 'ma', 'ox'],
    3: ['abs', 'ads', 'guy', 'axe'],
    4: ['girl', 'guys', 'axes'],
    5: ['girls'],
    6: ['flower'],
    7: ['flowers']
}

ADJ_LIST = ['bad', 'six', 'wiry', 'null', 'cloudy', 'tender', 'oafish', 'chilly', 'sexual', 'damaged']


class RandomVillagerCatchPhraseGenerator:

    @staticmethod
    def generate(words: int):
        for _ in range(0, words):
            adj = random.choice(ADJ_LIST)
            noun = random.choice(NOUN_DICT[10 - len(adj) - 1])
            print("%s %s" % (adj, noun))
