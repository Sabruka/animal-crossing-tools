import random

import numpy
import pygame as pygame


SAMPLE_RATE = 44100


class NoteCalculator:
    @staticmethod
    def sine_wave(hz, peak, n_samples=int(SAMPLE_RATE / 4)):
        """Compute N samples of a sine wave with given frequency and peak amplitude.
           Defaults to one second.
        """
        length = SAMPLE_RATE / float(hz)
        omega = numpy.pi * 2 / length
        xvalues = numpy.arange(int(length)) * omega
        onecycle = peak * numpy.sin(xvalues)
        return numpy.resize(onecycle, (n_samples,)).astype(numpy.int16)


EMPTY = "X"
RANDOM = "?"
CONTINUE = "_"
NOTES = ["g", "a", "b", "c", "d", "e", "f", "G", "A", "B", "C", "D", "E"]
EMPTY_WAVE = NoteCalculator.sine_wave(1, 4096)
NOTE_FUNCTION = {
    "g": NoteCalculator.sine_wave(329, 4096),
    "a": NoteCalculator.sine_wave(349, 4096),
    "b": NoteCalculator.sine_wave(392, 4096),
    "c": NoteCalculator.sine_wave(440, 4096),
    "d": NoteCalculator.sine_wave(493, 4096),
    "e": NoteCalculator.sine_wave(523, 4096),
    "f": NoteCalculator.sine_wave(587, 4096),
    "G": NoteCalculator.sine_wave(659, 4096),
    "A": NoteCalculator.sine_wave(698, 4096),
    "B": NoteCalculator.sine_wave(784, 4096),
    "C": NoteCalculator.sine_wave(880, 4096),
    "D": NoteCalculator.sine_wave(987, 4096),
    "E": NoteCalculator.sine_wave(1046, 4096)
}
FULL_NOTE_LENGTH = 280
PRINT_NOTE = False


class RandomTownTuneGenerator:

    @staticmethod
    def generate(number: int, autoplay: bool = True, base: str = '................'):
        for i in range(0, number):
            follow = False
            rand_index = None

            tune = ""

            note = random.choice(NOTES)
            if base[0] == '.':
                tune = tune + note
            else:
                tune = tune + base[0]

            for i in range(1, 16):
                if base[i] == '.':
                    if note != EMPTY and note != RANDOM and note != CONTINUE:
                        # 25% tiny delta, 50% chance small delta, 25% chance large delta
                        chance = random.randint(1, 100)
                        if chance < 25:
                            delta = random.randint(-1, 1)
                        elif chance < 75:
                            delta = random.randint(-2, 2)
                        else:
                            delta = random.randint(-5, 5)
                        note_index = NOTES.index(note) + delta
                        if note_index < 0:
                            note_index = 0
                        if note_index >= len(NOTES):
                            note_index = len(NOTES) - 1
                        note = NOTES[note_index]
                    else:
                        note = random.choice(NOTES)

                    # No more than 2 notes following each other
                    if not follow and note == tune[-1]:
                        follow = True
                    elif follow and note == tune[-1]:
                        while note == tune[-1]:
                            note = random.choice(NOTES)
                        follow = False

                    # If note is different than previous note, 5% chance of "continue" note.
                    if note != tune[-1] and random.randint(1, 100) < 5:
                        note = CONTINUE
                    # 5% chance of empty.
                    elif random.randint(1, 100) < 5:
                        note = EMPTY
                    # Note has small chance to be random if no random note has been found
                    # Random note only in last 4 notes
                    elif not rand_index and i > 11 and random.randint(1, 100) < 10:
                        rand_index = i

                    tune = tune + note

                else:
                    tune = tune + base[i]

            if rand_index:
                tune = list(tune)
                tune[rand_index] = RANDOM
                tune = "".join(tune)

            # Last note has a 20% chance of being a continue
            if random.randint(1, 100) < 20:
                tune = list(tune)
                tune[-1] = CONTINUE
                tune = "".join(tune)

            print("Generated tune: \n%s" % (tune))

            if autoplay:
                RandomTownTuneGenerator.play(tune)

    @staticmethod
    def play(tune: str):
        print("Playing town tune: %s" % tune)
        pygame.mixer.pre_init(SAMPLE_RATE, -16, 1, 32)
        pygame.mixer.init(SAMPLE_RATE, -16, 1, 32)
        i = 0
        waves = []
        while i < len(tune):
            m = 1
            note_code = tune[i]
            if PRINT_NOTE:
                print("[%i:%s]" % (i, note_code))

            while i < len(tune) - 1 and tune[i+1] == CONTINUE:
                m = m + 1
                i = i + 1

            if note_code == EMPTY:
                wave = EMPTY_WAVE
            elif note_code == RANDOM:
                wave = NOTE_FUNCTION[random.choice(list(NOTE_FUNCTION.keys()))]
            else:
                wave = NOTE_FUNCTION[note_code]

            for _ in range(0, m):
                waves.append(wave)
            i = i + 1

        full = numpy.concatenate(waves)

        RandomTownTuneGenerator.play_for(full, len(tune) * FULL_NOTE_LENGTH)

    @staticmethod
    def play_for(wave, ms):
        sound = pygame.sndarray.make_sound(wave)
        sound.play()
        pygame.time.delay(ms)
        sound.stop()
