import random


class RandomTreePlacementGenerator:

    @staticmethod
    def generate(width: int = 10, height: int = 10):
        grid = [['X' for _ in range(width)] for _ in range(height)]

        possible_coords = []
        for y in range(1, height - 1):
            for x in range(1, width - 1):
                possible_coords.append((x, y))

        while len(possible_coords) > 0:
            x, y = random.choice(possible_coords)
            grid[y][x] = "O"
            for y2 in range(y - 1 if y - 1 > 0 else 0, y + 2 if y + 2 < height else height):
                for x2 in range(x - 1 if x - 1 > 0 else 0, x + 2 if x + 2 < width else width):
                    if (x2, y2) in possible_coords:
                        possible_coords.remove((x2, y2))

        for row in grid:
            print("".join(row))

