import random
import sys

from tools.random_catch_phrase import RandomVillagerCatchPhraseGenerator
from tools.random_town_tune import RandomTownTuneGenerator
from tools.random_tree_placement_generator import RandomTreePlacementGenerator


def main(argv):
    tool = argv[0]
    if tool == 'rttg':
        if len(argv) > 1 and argv[1] == 'play':
            RandomTownTuneGenerator.play(argv[2])
        elif len(argv) > 1 and argv[1] == 'base':
            RandomTownTuneGenerator.generate(1, base=argv[2])
        else:
            RandomTownTuneGenerator.generate(int(argv[1]) if len(argv) > 1 else 1)
    if tool == 'rvcpg':
        RandomVillagerCatchPhraseGenerator.generate(int(argv[1]) if len(argv) > 1 else 10)
    if tool == 'fdcg':
        avail = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0']
        code = ""
        for _ in range(0, 5):
            code = code + random.choice(avail)
        print("Your Fake DodoCode: %s" % code)
    if tool == 'rtpg':
        if len(argv) > 2:
            RandomTreePlacementGenerator.generate(int(argv[1]), int(argv[2]))
        else:
            RandomTreePlacementGenerator.generate()
    else:
        print("Valid tools: rttg, rvcpg, fdcg.")


if __name__ == "__main__":
   main(sys.argv[1:])